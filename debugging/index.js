document.addEventListener('DOMContentLoaded', function() {
    const button = document.querySelector('#main-button');
    let numClicks = 0;

    button.onclick = function() {
        numClicks++;

        if (numClicks > 3) {
            alert('Clicked 3 times!');
            numClicks = 0;
        }

        const numClicksLabel = document.querySelector('#num-click');
        numClicksLabel.textContent = 3 - numClicks;
    };
});

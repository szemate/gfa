function getMutualElements(list1, list2) {
    const mutualElements = [];

    for (let i = 0; i < list1.length; i++) {
        if (list2.includes(list1[i]) && mutualElements.includes(list1[i])) {
            mutualElements.append(list1[i]);
        }
    }

    return mutualElements;
}

console.log(getMutualElements([1, 2, 3], [2, 3, 4]));

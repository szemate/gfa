const express = require('express');
const path = require('path');

const port = 8080;
const app = express();

app.use('/static', express.static(path.join(__dirname, 'public')));

app.get('/', (req, res) => {
  res.sendFile(path.join(__dirname, 'public', 'index.html'));
});

app.use('/api/*', (req, res) => {
  // Return 404 errors for the REST API in JSON format
  res.status(404).send({ message: 'Not found' });
});

app.listen(port, () => console.log(`Server running on port ${port}`));

const pageSize = 10;
let currentPage = 1;

const templateCache = {};

const renderTemplate = async (template, data) => {
  if (templateCache[template] === undefined) {
    const response = await fetch(template);
    templateCache[template] = await response.text();
  }

  return ejs.render(templateCache[template], data);
};

const fetchData = async (method, resource, requestData) => {
  const response = await fetch(resource, {
    method,
    body: requestData ? JSON.stringify(requestData) : undefined,
    headers: requestData ? { 'Content-Type': 'application/json' } : {},
  });

  clearError();

  if (response.status === 204) {
    return {};
  }

  const responseData = await response.json();

  if (!response.ok) {
    showError(`${method} ${resource}`, responseData.message);
    return null;
  }

  return responseData;
};

const showError = async (context, message) => {
  document.querySelector('#error').innerHTML = await renderTemplate(
    '/static/error.ejs',
    { context, message },
  );
};

const clearError = () => {
  document.querySelector('#error').innerHTML = '';
};

const getSelectedParticipantId = () => document.querySelector(
  '.participant-radio:checked',
)?.value;

const getParticipants = async () => {
  const data = await fetchData(
    'GET',
    `/api/participants?page=${currentPage}&size=${pageSize}`,
  );
  if (!data) {
    return;
  }

  const selectedId = getSelectedParticipantId();

  document.querySelector('#participant-rows').innerHTML = await renderTemplate(
    '/static/tbody.ejs',
    { participants: data.participants },
  );

  const radioButtonToSelect = (
    document.querySelector(`.participant-radio[value="${selectedId}"]`)
    || document.querySelector('.participant-radio:first-child')
  );
  radioButtonToSelect.checked = true;

  if (data.pages) {
    addPagination(data.pages);
  }
};

const addPagination = (numPages) => {
  const paginator = document.querySelector('#pagination');
  paginator.innerHTML = '';

  for (let i = 1; i <= numPages; i++) {
    const btn = document.createElement('button');
    btn.classList.add('btn', i === currentPage ? 'btn-dark' : 'btn-light');
    btn.textContent = i;
    btn.onclick = () => {
      currentPage = i;
      getParticipants();
    };
    paginator.appendChild(btn);
  }
};

const getClasses = async (currentId) => {
  const data = await fetchData('GET', '/api/classes');
  if (!data) {
    return;
  }

  const selector = document.querySelector('#class');

  data.classes.forEach((cls) => {
    const option = document.createElement('option');
    option.textContent = cls.name;
    option.value = cls.id;
    option.selected = cls.id === currentId;
    selector.appendChild(option);
  });
};

const closeEditor = () => {
  document.querySelector('#editor-container').innerHTML = '';
};

const openEditor = async (title, handleSubmit, participant = {}) => {
  closeEditor();

  document.querySelector('#editor-container').innerHTML = await renderTemplate(
    '/static/editor.ejs',
    { title, participant },
  );
  document.querySelector('#cancel-button').onclick = handleClose;
  document.querySelector('#submit-button').onclick = handleSubmit;

  getClasses(participant.class_id);
};

const createSubmitHandler = (method, resource) => async (event) => {
  event.preventDefault();

  const requestData = {
    first_name: document.querySelector('#first-name').value,
    last_name: document.querySelector('#last-name').value,
    email: document.querySelector('#email').value,
    phone_number: document.querySelector('#phone-number').value,
    class_id: Number(document.querySelector('#class').value),
  };

  if (await fetchData(method, resource, requestData)) {
    closeEditor();
    getParticipants();
  }
};

const handleClose = (event) => {
  event.preventDefault();
  closeEditor();
};

const handleAdd = (event) => {
  event.preventDefault();
  openEditor(
    'Add Participant',
    createSubmitHandler('POST', '/api/participants'),
  );
};

const handleEdit = async (event) => {
  event.preventDefault();

  const selectedId = getSelectedParticipantId();
  if (!selectedId) {
    return;
  }

  const resource = `/api/participants/${selectedId}`;
  const participant = await fetchData('GET', resource);

  openEditor(
    'Edit Participant',
    createSubmitHandler('PUT', resource),
    participant || {},
  );
};

const handleDelete = async (event) => {
  event.preventDefault();
  const selectedId = getSelectedParticipantId();

  if (!selectedId || !window.confirm('Are you sure?')) {
    return;
  }

  if (await fetchData('DELETE', `/api/participants/${selectedId}`)) {
    getParticipants();
  }
};

window.addEventListener('DOMContentLoaded', () => {
  document.querySelector('#add-button').onclick = handleAdd;
  document.querySelector('#edit-button').onclick = handleEdit;
  document.querySelector('#delete-button').onclick = handleDelete;

  getParticipants();
});

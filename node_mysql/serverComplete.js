const express = require('express');
const mysql = require('mysql2');
const path = require('path');

const port = 8080;
const app = express();

const db = mysql.createConnection({
  host: '127.0.0.1',
  user: 'root',
  password: 'abc123',
  database: 'gfa_register',
});

db.connect((err) => {
  if (err) {
    console.error(err);
    return;
  }
  console.log('Connection established');
});

const validateParticipant = (participant) => {
  if (participant.first_name === '') {
    throw new Error('First name cannot be empty');
  }
  if (participant.last_name === '') {
    throw new Error('Last name cannot be empty');
  }
};

app.use('/static', express.static(path.join(__dirname, 'public')));

app.use(express.json());

app.get('/', (req, res) => {
  res.sendFile(path.join(__dirname, 'public', 'index.html'));
});

app.get('/api/participants', (req, res) => {
  const page = Number(req.query.page) || 1;
  const pageSize = Number(req.query.size) || 10;

  const query = `
    SELECT participants.*,
           classes.name AS class,
           cohorts.name AS cohort
    FROM participants
    JOIN classes
    ON participants.class_id = classes.id
    JOIN cohorts
    ON classes.cohort_id = cohorts.id
    ORDER BY participants.last_name
    LIMIT ?
    OFFSET ?
  `;
  const params = [pageSize, (page - 1) * pageSize];

  db.query(query, params, (err, rows) => {
    if (err) {
      console.error(err);
      res.status(500).send({ message: 'Database Error' });
      return;
    }

    const query2 = 'SELECT COUNT(*) AS count FROM participants';
    db.query(query2, (err2, rows2) => {
      if (err2) {
        console.error(err2);
        res.status(500).send({ message: 'Database Error' });
        return;
      }

      res.send({
        participants: rows,
        pages: Math.ceil(rows2[0].count / pageSize),
      });
    });
  });
});

app.get('/api/participants/:id', (req, res) => {
  const query = 'SELECT * FROM participants WHERE id = ?';
  const params = [Number(req.params.id)];

  db.query(query, params, (err, rows) => {
    if (err) {
      console.error(err);
      res.status(500).send({ message: 'Database Error' });
      return;
    }
    if (rows.length === 0) {
      res.status(404).send({ message: 'Not found' });
      return;
    }
    res.send(rows[0]);
  });
});

app.post('/api/participants', (req, res) => {
  try {
    validateParticipant(req.body);
  } catch (error) {
    res.status(400).send({ message: error.message });
    return;
  }

  const query = `
    INSERT INTO participants (first_name, last_name, email, phone_number, class_id)
    VALUES (?, ?, ?, ?, ?)
  `;
  const params = [
    req.body.first_name,
    req.body.last_name,
    req.body.email,
    req.body.phone_number,
    req.body.class_id,
  ];

  db.query(query, params, (err) => {
    if (err) {
      console.error(err);
      res.status(500).send({ message: 'Database error' });
      return;
    }

    res.status(201).send({ message: 'Created' });
  });
});

app.put('/api/participants/:id', (req, res) => {
  try {
    validateParticipant(req.body);
  } catch (error) {
    res.status(400).send({ message: error.message });
    return;
  }

  const query = `
    UPDATE participants
    SET first_name = ?, last_name = ?, email = ?, phone_number = ?, class_id = ?
    WHERE id = ?
  `;
  const params = [
    req.body.first_name,
    req.body.last_name,
    req.body.email,
    req.body.phone_number,
    req.body.class_id,
    Number(req.params.id),
  ];

  db.query(query, params, (err, result) => {
    if (err) {
      console.error(err);
      res.status(500).send({ message: 'Database Error' });
      return;
    }

    if (result.affectedRows === 0) {
      res.status(404).send({ message: 'Not Found' });
      return;
    }

    res.status(204).send();
  });
});

app.delete('/api/participants/:id', (req, res) => {
  const query = 'DELETE FROM participants WHERE id = ?';
  const params = [Number(req.params.id)];

  db.query(query, params, (err) => {
    if (err) {
      console.error(err);
      res.status(500).send({ message: 'Database Error' });
      return;
    }

    res.status(204).send();
  });
});

app.get('/api/classes', (req, res) => {
  const query = 'SELECT * FROM classes ORDER BY name';

  db.query(query, (err, rows) => {
    if (err) {
      console.error(err);
      res.status(500).send({ message: 'Database Error' });
      return;
    }
    res.send({ classes: rows });
  });
});

app.use('/api/*', (req, res) => {
  // Return 404 errors for the REST API in JSON format
  res.status(404).send({ message: 'Not found' });
});

app.listen(port, () => console.log(`Server running on port ${port}`));

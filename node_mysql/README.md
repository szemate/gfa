# GFA Register

## API Specification

### `GET /api/participants?page={x}&size={y}`

Returns the list of participants with pagination in alphabetical order by last
name.

#### Query string parameters

- `page`: The 1-based index of the page
- `size`: The number of participants per page

#### Responses

- **200 OK:** Successfully returned a list of participants (may be empty)

    ```json
    {
      "pages": 1,
      "participants": [
        {
          "id": 1,
          "first_name": "György",
          "last_name": "Szántó",
          "email": "gyszanto@gmail.com"
          "phone_number": "+36709873466",
          "cohort": "VULPES",
          "class": "Chama"
        }
      ]
    }
    ```

---

### `GET /api/participants/{id}`

Returns a single participant.

#### Responses

- **200 OK:** Successfully returned a participant

    ```json
    {
      "id": 1,
      "first_name": "György",
      "last_name": "Szántó",
      "email": "gyszanto@gmail.com",
      "phone_number": "+36709873466",
      "cohort": "VULPES",
      "class": "Chama"
    }
    ```

- **404 Not Found:** No participant found with the specified ID

    ```json
    {
      "message": "Not Found"
    }
    ```

---

### `POST /api/participants`

Adds a new participant.

#### Request

```json
{
  "first_name": "György",
  "last_name": "Szántó",
  "email": "gyszanto@gmail.com",
  "phone_number": "+36709873466",
  "class_id": 1
}
```

#### Responses

- **201 Created:** Successfully created the participant

    ```json
    {
      "id": 1
    }
    ```

- **400 Bad Request:** Some of the request data is missing or invalid

    ```json
    {
      "message": "first_name connot be empty"
    }
    ```

---

### `PUT /api/participants/{id}`

Updates a participant.

#### Request

```json
{
  "first_name": "György",
  "last_name": "Szántó",
  "email": "gyszanto@gmail.com",
  "phone_number": "+36709873466",
  "class_id": 1
}
```

#### Responses

- **204 No Content:** Successfully updated the participant

- **400 Bad Request:** Some of the request data is missing or invalid

    ```json
    {
      "message": "first_name connot be empty"
    }
    ```

- **404 Not Found:** No participant found with the specified ID

    ```json
    {
      "message": "Not Found"
    }
    ```

---

### `DELETE /api/participants/{id}`

Deletes a participant.

#### Responses

- **204 No Content:** The participant has been deleted

---

### `GET /api/classes`

Returns the list of classes in alphabetical order.

#### Responses

- 200 OK: Successfully returned a list of classes (may be empty)

    ```json
    {
      "classes": [
        {
          "id": 1,
          "name": "Chama"
        }
      ]
    }
    ```

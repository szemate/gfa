INSERT INTO cohorts (name) VALUES ('VULPES'), ('VELOX');

INSERT INTO classes (name, cohort_id)
VALUES ('Chama', 1), ('Griseus', 2), ('Vetulus', 1);

INSERT INTO participants (first_name, last_name, email, phone_number, class_id)
VALUES
    ('György', 'Szántó','gyszanto@gmail.com', '+36709873466', 1),
    ('Olga', 'Faragó', 'ofarago@yahoo.com', '+36706353246', 3),
    ('Zsófia', 'Pusztai', 'zspusztai@gmail.com', '+36309546765', 1),
    ('Adolf', 'Czinege','aczinege@gmail.com', '+36309553321', 2),
    ('Rita', 'Dobos','rdobos@yahoo.com', '+36303648237', 3),
    ('Luca', 'Kárpáti','lkarpati@gmail.com', '+36205528647', 1),
    ('Károly', 'Görög','kgorog@outlook.com', '+36208493286', 3),
    ('Emil', 'Horváth','ehorvath@gmail.com', '+36303232753', 2),
    ('Bernadett', 'Ruzsa','bruzsa@yahoo.com', '+36708785746', 3),
    ('János', 'Tar','jtar@gmail.com', '+36203664778', 1),
    ('Zsófia', 'Bálint','zsbalint@gmail.com', '+36203856634', 2),
    ('Rudolf', 'Kató','rkato@gmail.com', '+36308844367', 2),
    ('Eszter', 'Pataki','epataki@yahoo.com', '+36704728844', 1),
    ('Krisztofer', 'Vida','kvida@gmail.com', '+36309338477', 2),
    ('Károly', 'Takács','ktakacs@outlook.com', '+36203558761', 3),
    ('Szonja', 'Vörös','szvoros@gmail.com', '+36208575467', 3);

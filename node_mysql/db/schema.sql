DROP TABLE IF EXISTS participants;
DROP TABLE IF EXISTS classes;
DROP TABLE IF EXISTS cohorts;

CREATE TABLE cohorts (
  id   INT AUTO_INCREMENT,
  name VARCHAR(120) NOT NULL,
  PRIMARY KEY (id)
);

CREATE TABLE classes (
  id         INT AUTO_INCREMENT,
  name       VARCHAR(120) NOT NULL,
  cohort_id  INT NOT NULL,
  PRIMARY KEY (id),
  FOREIGN KEY (cohort_id) REFERENCES cohorts(id)
);

CREATE TABLE participants (
  id           INT AUTO_INCREMENT,
  first_name   VARCHAR(30) NOT NULL,
  last_name    VARCHAR(30) NOT NULL,
  email        VARCHAR(30),
  phone_number VARCHAR(30),
  class_id     INT NOT NULL,
  PRIMARY KEY (id),
  FOREIGN KEY (class_id) REFERENCES classes(id),
  UNIQUE (email),
  UNIQUE (phone_number)
);

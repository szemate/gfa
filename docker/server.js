const express = require('express');

const port = 3000;
const app = express();

app.use(express.static('public'));
app.use(express.urlencoded());

app.get('/', (req, res) => {
    res.redirect('/index.html');
});

app.post('/sign-up', (req, res) => {
    const { name, email, phone, password } = req.body;
    if (!name || !email || !phone || !password) {
        res.status(400).sendFile(__dirname + '/public/error.html');
    }
    res.redirect('/success.html');
});

app.listen(port, () => console.log(`Server listening on port ${port}`));

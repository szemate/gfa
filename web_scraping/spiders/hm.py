import scrapy
from .. import items


class HmSpider(scrapy.Spider):
    name = 'hm'
    allowed_domains = ['www2.hm.com']
    start_urls = ['https://www2.hm.com/en_gb/ladies/shop-by-product/trousers.html']  # pylint: disable=line-too-long

    def parse(self, response):
        for elem in response.css('.item-details'):
            name = elem.css('.item-heading > a::text').get()
            price = elem.css('.price.regular::text').get()
            cleaned_price = float(price.replace('£', ''))
            item = items.ShoppingItem(name=name, price=cleaned_price)
            yield item

        next_page = response.css('a.pagination-links-list::attr(href)').get()
        if next_page is not None:
            yield response.follow(next_page, callback=self.parse)

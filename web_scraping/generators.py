# List

numbers = [n**2 for n in range(5)]
for n in numbers:
    print(n)

# Iterator

numbers = iter(n**2 for n in range(5))
while True:
    try:
        print(next(numbers))
    except StopIteration:
        break

numbers = iter(n**2 for n in range(5))
for n in numbers:
    print(n)
for n in numbers:
    print(n)

# Generator

def genNumbers():
    for x in range(5):
        yield x**2
    yield -1

numbers = genNumbers()
for n in numbers:
    print(n)

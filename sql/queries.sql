-- Kik a résztvevők, milyen osztályok és cohortok vannak?
SELECT first_name, last_name FROM participants;
SELECT name FROM classes;
SELECT name FROM cohorts;

-- Kik a résztvevők, névsor szerint rendezve?
SELECT first_name, last_name FROM participants ORDER BY last_name ASC;

-- Melyik osztályok vannak a VULPES cohortban?
SELECT name, id FROM cohorts WHERE name = 'VULPES';
SELECT name FROM classes WHERE cohort_id = 1;

-- Kik azok a résztvevők, akiknek GMailes email címe van?
SELECT first_name, last_name, email FROM participants WHERE email LIKE '%gmail.com';

-- Melyik osztályok futottak 2020 május 1-én?
SELECT name FROM classes WHERE start_date <= '2020-05-01' AND end_date >= '2020-05-01';

-- Milyen cohortok vannak, a hozzájuk tartozó osztályokkal együtt?
SELECT cohorts.name AS cohort_name, classes.name AS class_name
FROM cohorts
INNER JOIN classes ON classes.cohort_id = cohorts.id;

-- Milyen cohortok vannak, a hozzájuk tartozó osztályokkal együtt,
-- beleértve azokat is, amelyekhez nem tartozik osztály?
SELECT cohorts.name AS cohort_name, classes.name AS class_name
FROM cohorts
LEFT OUTER JOIN classes ON classes.cohort_id = cohorts.id;

-- Kik a résztvevők, a cohortjukkal és az osztályukkal együtt?
SELECT participants.first_name, participants.last_name, cohorts.name, classes.name
FROM participants
JOIN participants_classes ON participants_classes.participant_id = participants.id
JOIN classes ON participants_classes.class_id = classes.id
JOIN cohorts ON classes.cohort_id = cohorts.id;

-- Kik azok a résztvevők, akik a VULPES cohortban vannak?
SELECT participants.first_name, participants.last_name, cohorts.name, classes.name
FROM participants
JOIN participants_classes ON participants_classes.participant_id = participants.id
JOIN classes ON participants_classes.class_id = classes.id
JOIN cohorts ON classes.cohort_id = cohorts.id
WHERE cohorts.name = 'VULPES';

-- Kik azok a résztvevők, akik a Vetulus vagy a Griseus osztályban vannak?
SELECT participants.first_name, participants.last_name, cohorts.name, classes.name
FROM participants
JOIN participants_classes ON participants_classes.participant_id = participants.id
JOIN classes ON participants_classes.class_id = classes.id
JOIN cohorts ON classes.cohort_id = cohorts.id
WHERE classes.name IN ('Vetulus', 'Griseus');

-- Milyen különböző keresztnevű résztvevők vannak?
SELECT DISTINCT first_name, last_name FROM participants;

-- Hány mentor van?
SELECT COUNT(participants.id) FROM participants
WHERE is_mentor IS true;

-- Hány mentor tanít a VULPES cohortban?
SELECT COUNT(participants.id)
FROM participants
JOIN participants_classes ON participants_classes.participant_id = participants.id
JOIN classes ON participants_classes.class_id = classes.id
JOIN cohorts ON classes.cohort_id = cohorts.id
WHERE cohorts.name = 'VULPES' AND participants.is_mentor IS true;

-- Melyik osztály fejezi be a képzést utoljára?
SELECT MAX(end_date) FROM classes;
-- Alternatív megoldás
SELECT name, end_date FROM classes ORDER BY end_date DESC LIMIT 1;

-- Melyik cohortban hány mentor tanít?
SELECT COUNT(participants.id), cohorts.name
FROM participants
JOIN participants_classes ON participants_classes.participant_id = participants.id
JOIN classes ON participants_classes.class_id = classes.id
JOIN cohorts ON classes.cohort_id = cohorts.id
WHERE participants.is_mentor IS true
GROUP BY cohorts.id;

-- Hány különböző keresztnevű résztvevő van a képzéseken?
SELECT COUNT(DISTINCT first_name) FROM participants;

-- Nevezzük át a Griseus osztályt Avius-ra!
UPDATE classes SET name = 'Avius' WHERE name = 'Griseus';
SELECT name FROM classes;

-- Töröljük ki Dobos Ritát a résztvevők közül!
SELECT id FROM participants WHERE first_name = 'Rita' AND last_name = 'Dobos';
DELETE FROM participants_classes WHERE participant_id = 5;
DELETE FROM participants WHERE id = 5;

-- Extra: Ki az a résztvevő, akinek nincs osztálya?

// Euclidean algorithm
// https://www.khanacademy.org/computing/computer-science/cryptography/modarithmetic/a/the-euclidean-algorithm

function gcd(a: number, b: number): number {
  // Input validation
  if (Math.floor(a) !== a || Math.floor(b) !== b) {
    throw new Error('the inputs must be integers');
  }

  // Edge case: handling integers
  a = Math.abs(a);
  b = Math.abs(b);

  // Base case
  if (b === 0) {
    return a;
  }

  // Recursive call
  const modulo: number = a % b;
  return gcd(b, modulo);
}

console.log(gcd(1071, 462)); // 21
console.log(gcd(-1071, -462)); // 21

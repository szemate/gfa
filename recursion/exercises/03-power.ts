function power(base: number, n: number): number {
  // Input validation
  if (n < 0 || Math.floor(n) !== n) {
    throw new Error('n must be a positive integer');
  }

  // Base case
  if (n === 1) {
    return base;
  }

  // Recursive call
  return base * power(base, n - 1);
}

console.log(power(2, 5));

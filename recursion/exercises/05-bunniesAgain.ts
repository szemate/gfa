function calculateEarsAgain(numOfBunnies: number): number {
  // Input validation
  if (numOfBunnies < 0 || Math.floor(numOfBunnies) !== numOfBunnies) {
    throw new Error('numOfBunnies must be a positive integer');
  }

  // Base case
  if (numOfBunnies === 0) {
    return 0;
  }

  // Recursive call
  const numOfEars: number = numOfBunnies % 2 === 0 ? 3 : 2;
  return numOfEars + calculateEarsAgain(numOfBunnies - 1);
}

console.log(calculateEarsAgain(5));

// Trivial solution

function findMaxSum(numbers: number[], subsetSize: number): number | null {
  // Edge case
  if (numbers.length === 0) {
    return null;
  }

  const largestNumbers: number[] = numbers
    .sort(compareDescending)
    .slice(0, subsetSize);

  return calculateSum(largestNumbers);
}

function compareDescending(a: number, b: number): number {
  return b - a;
}

function calculateSum(numbers: number[]): number {
  let sum = 0;

  for (let i: number = 0; i < numbers.length; i++) {
    sum += numbers[i];
  }

  return sum;
}

console.log(findMaxSum([1, 2, 3, 4, 5], 4));

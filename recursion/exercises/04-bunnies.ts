function calculateEars(numOfBunnies: number): number {
  // Input validation
  if (numOfBunnies < 0 || Math.floor(numOfBunnies) !== numOfBunnies) {
    throw new Error('numOfBunnies must be a positive integer');
  }

  // Base case
  if (numOfBunnies === 0) {
    return 0;
  }

  // Recursive call
  return 2 + calculateEars(numOfBunnies - 1);
}

console.log(calculateEars(5));

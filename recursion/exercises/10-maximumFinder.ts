function findMaximum(numbers: number[]): number | null {
  // Edge case
  if (numbers.length === 0) {
    return null;
  }

  const firstNum: number = numbers[0];

  // Base case
  if (numbers.length === 1) {
    return firstNum;
  }

  // Recursive call
  const remainingNums: number[] = numbers.splice(1);
  const maxOfRemainingNums: number = findMaximum(remainingNums) as number;
  return maxOfRemainingNums > firstNum ? maxOfRemainingNums : firstNum;
}

console.log(findMaximum([1, 3, 5, 7, 6, 4, 2]));

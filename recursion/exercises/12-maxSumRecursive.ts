// Recursive solution

function findMaxSum(numbers: number[], subsetSize: number): number | null {
  // Edge case
  if (numbers.length === 0) {
    return null;
  }

  // Generate all combinations
  const combinations: number[][] = [];
  generateCombinations(numbers, subsetSize, combinations);
  console.table(combinations);

  // Calculate all sums
  const sums: number[] = combinations.map(calculateSum);
  console.debug('sums:', sums);

  // Find the largest sum
  return Math.max(...sums);
}

// Backtracking algorithm for generating all combinations recursively
// https://www.enjoyalgorithms.com/blog/find-all-combinations-of-k-numbers
function generateCombinations(
  numbers: number[],
  subsetSize: number,
  combinations: number[][],
  solutionVector: number[] = [],
  startIndex: number = 0,
): void {
  // Base case
  if (subsetSize === 0) {
    const completedCombination: number[] = [...solutionVector]; // clone
    combinations.push(completedCombination);
    return;
  }

  for (let i: number = startIndex; i < numbers.length; i++) {
    solutionVector.push(numbers[i]);
    console.debug('push:', subsetSize, solutionVector);

    // Recursive call
    generateCombinations(
      numbers,
      subsetSize - 1,
      combinations,
      solutionVector,
      i + 1
    );

    // Backtracking
    solutionVector.pop();
    console.debug('backtrack:', subsetSize, solutionVector);
  }
}

function calculateSum(numbers: number[]): number {
  return numbers.reduce((sum: number, n: number): number => sum + n);
}

console.log('max sum:', findMaxSum([1, 2, 3, 4, 5], 4));

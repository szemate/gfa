function sumUntil(n: number): number {
  // Input validation
  if (n < 0 || Math.floor(n) !== n) {
    throw new Error('n must be a positive integer');
  }

  // Base case
  if (n === 0) {
    return 0;
  }

  // Recursive call
  return n + sumUntil(n - 1);
}

console.log(sumUntil(5));

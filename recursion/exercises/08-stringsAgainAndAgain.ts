function addAsterisks(word: string): string {
  // Edge case
  if (word === '') {
    return '';
  }

  // Base case
  if (word.length === 1) {
    return word;
  }

  // Recursive call
  let firstChar: string = word.charAt(0);
  const remainingChars: string = word.substr(1);
  return firstChar + '*' + addAsterisks(remainingChars);
}

console.log(addAsterisks('word'));

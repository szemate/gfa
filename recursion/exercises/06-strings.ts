function swapXwithY(word: string): string {
  // Edge case
  if (word === '') {
    return '';
  }

  let firstChar: string = word.charAt(0);
  if (firstChar === 'x') {
    firstChar = 'y';
  }

  // Base case
  if (word.length === 1) {
    return firstChar;
  }

  // Recursive call
  const remainingChars: string = word.substr(1);
  return firstChar + swapXwithY(remainingChars);
}

console.log(swapXwithY('xenophobic tuxedo sex'));

function fibonacci(n: number): number {
  // Input validation
  if (n < 0 || Math.floor(n) !== n) {
    throw new Error('n must be a positive integer');
  }

  // Base cases
  if (n <= 1) {
    return n;
  }

  // Recursive call
  return fibonacci(n - 1) + fibonacci(n - 2);
}

console.log(fibonacci(8));

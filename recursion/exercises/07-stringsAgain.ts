function removeXs(word: string): string {
  // Edge case
  if (word === '') {
    return '';
  }

  let firstChar: string = word.charAt(0);
  if (firstChar === 'x') {
    firstChar = '';
  }

  // Base case
  if (word.length === 1) {
    return firstChar;
  }

  // Recursive call
  const remainingChars: string = word.substr(1);
  return firstChar + removeXs(remainingChars);
}

console.log(removeXs('xenophobic tuxedo sex'));

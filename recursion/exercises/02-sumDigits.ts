function sumDigits(n: number): number {
  // Input validation
  if (n < 0 || Math.floor(n) !== n) {
    throw new Error('n must be a positive integer');
  }

  const lastDigit: number = n % 10;

  // Base case
  const isSingleDigit: boolean = lastDigit === n;
  if (isSingleDigit) {
    return n;
  }

  // Recursive call
  const remainingDigits: number = Math.floor(n / 10);
  return lastDigit + sumDigits(remainingDigits);
}

console.log(sumDigits(12345));

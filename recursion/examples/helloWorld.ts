function helloWorld(n: number): void {
  if (n === 0) {
    return;
  }
  console.log('Hello World!');
  helloWorld(n - 1);
}

helloWorld(5);

function printPositiveNumbersUntil(n: number): void {
  if (n === 0) {
    return;
  }
  printPositiveNumbersUntil(n - 1);
  console.log(n);
}

printPositiveNumbersUntil(5);

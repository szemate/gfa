export {};

const numbers: number[] = [3, 6, 9, 12, 15];

function print(array: number[]): void {
  array = [...array];
  if (array.length === 0) {
    return;
  }
  console.log(array[0]);
  print(array.splice(1));
}

print(numbers);

function find(array: number[], numberToFind: number): number | null {
  array = [...array];
  if (array.length === 0) {
    return null;
  }
  if (array[0] === numberToFind) {
    return array[0];
  }
  return find(array.splice(1), numberToFind);
}

console.log(find(numbers, 12));
console.log(find(numbers, 13));

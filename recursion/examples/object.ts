export {};

type ValueType = string | number | boolean;
type ObjectType = {[key: string]: ValueType | ObjectType};

// https://bkkfutar.docs.apiary.io/#reference/0/routedetails/tripdetails
const tripDetails: ObjectType = {
  entry: {
    tripId: 'BKK_B132611',
    serviceDate: '20140920',
    vehicle: {
      vehicleId: 'BKK_352',
      stopId: 'BKK_F02196',
      routeId: 'BKK_0270',
      location: {
        lat: 47.4779,
        lon: 19.045807,
      },
      serviceDate: '20140920',
      licensePlate: 'BPI393',
      label: 'Ikarus 405-ös típusú autóbusz',
      deviated: true,
      lastUpdateTime: 1411241696,
      status: 'IN_TRANSIT_TO',
      stopDistancePercent: 0,
      tripId: 'BKK_B13261173',
    },
    stopTime: {
      stopId: 'BKK_F02196',
      stopHeadsign: 'Sánc utca',
      departureTime: 1411182120,
      predictedDepartureTime: 1411182136,
    },
  },
};

function print(obj: ObjectType): void {
  const keys = Object.keys(obj);

  for (const key of keys) {
    const value = obj[key];
    if (typeof value === 'object') {
      print(value);
    } else {
      console.log(key, value);
    }
  }
}

console.log(print(tripDetails));

function find(obj: ObjectType, keyToFind: string): ValueType | null {
  const keys = Object.keys(obj);

  for (const key of keys) {
    if (key === keyToFind) {
      return obj[key] as ValueType;
    }

    const value = obj[key];
    if (typeof value === 'object') {
      const result = find(value, keyToFind);
      if (result !== null) {
        return result;
      }
    }
  }

  return null;
}

console.log(find(tripDetails, 'stopHeadsign'));

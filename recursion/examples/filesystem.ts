import fs from 'fs';
import path from 'path';

export {};

const repoRootPath: string = getFullPath(__dirname, '..', '..', '..', '..');

function getDirectoryContents(path: string): string[] {
  return fs.readdirSync(path);
}

function isDirectory(path: string): boolean {
  return fs.statSync(path).isDirectory();
}

function getFullPath(...parts: string[]): string {
  return path.normalize(path.join(...parts));
}

function print(rootPath: string): void {
  const contents = getDirectoryContents(rootPath);

  for (const name of contents) {
    const fullPath = getFullPath(rootPath, name);

    if (isDirectory(fullPath)) {
      print(fullPath);
    } else {
      console.log(fullPath);
    }
  }
}

print(repoRootPath);

function findRecursive(rootPath: string, nameToFind: string): string | null {
  const contents = getDirectoryContents(rootPath);

  for (const name of contents) {
    if (name === nameToFind) {
      return name;
    }

    const fullPath = getFullPath(rootPath, name);

    if (isDirectory(fullPath)) {
      const result = findRecursive(fullPath, nameToFind);
      if (result !== null) {
        return result;
      }
    }
  }

  return null;
}

function findNonRecursive(rootPath: string, nameToFind: string): string | null {
  const dirsToTraverse: string[] = [rootPath];

  while (dirsToTraverse.length > 0) {
    const dir = dirsToTraverse.shift() as string;
    console.log(dir);
    const contents = getDirectoryContents(dir);

    for (const name of contents) {
      const fullPath = getFullPath(dir, name);

      if (name === nameToFind) {
        return fullPath;
      }

      if (isDirectory(fullPath)) {
        dirsToTraverse.push(fullPath);
      }
    }
  }

  return null;
}

console.log(findRecursive(repoRootPath, 'filesystem.ts'));
console.log(findNonRecursive(repoRootPath, 'filesystem.ts'));

# Reguláris kifejezések

### Speciális karakterek

- `.`: bármilyen karakter
- `\s`: space
- `\d`: számjegy
- `\w`: alfanumerikus karakter (angol ABC kis- és nagybetűi, számjegyek és `_`)
- `\p{L}`: mint a `\w`, de Unicode (ékezetes) karakter is lehet
- `[asd]`: karakter halmaz (`a` vagy `s` vagy `d`)
- `[a-d]`: karakter intervallum (`a` vagy `b` vagy `c` vagy `d`)
- `[^as]`: inverz karakter halmaz (bármilyen karakter ami nem `a` vagy `s`)

### Előfordulások száma

- `?`: 0 vagy 1
- `*`: 0 vagy több
- `+`: 1 vagy több
- `{1,3}`: intervallum (1 vagy 2 vagy 3)
- `{2,}`: végtelen intervallum (2 vagy több)

### Kikötések

- `^`: a sztring eleje
- `$`: a sztring vége

### Paraméterek

- `//g`: globális reguláris kifejezés (egy sztringben több találat)
- `//u`: Unicode (ékezetes) karaktert tartalmazó kifejezés

### Metódusok

- `String.match(RegExp)`: első találat megkeresése
- `String.matchAll(RegExp)`: összes találat megkeresése (csak globális
  kifejezéssel működik)
- `String.replace(RegExp, replacement)`: találat(ok) cseréje egy másik sztringre

function isMultipleOfFive(x) {
    return x % 3 === 0;
}

const numbers = [7, 8, 9, 10, 11];

const num = numbers.find(isMultipleOfFive);
console.log(num);

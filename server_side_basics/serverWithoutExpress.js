'use strict';

const fs = require('fs');
const http = require('http');
const path = require('path');

const app = http.createServer(function(req, res) {
  console.log(req.method, req.url);

  if (req.method === 'GET' && req.url === '/hello') {
    try {
      const content = fs.readFileSync(
        path.join(__dirname, 'public', 'helloWorld.html'),
        'utf-8'
      );
      res.writeHead(200, {'Content-Type': 'text/html'});
      res.end(content);
    } catch (err) {
      console.log(err);
      res.writeHead(500, {'Content-Type': 'text/plain'});
      res.end('Internal Server Error');
    }
  } else if (req.method === 'GET' && req.url === '/static/style.css') {
    try {
      const content = fs.readFileSync(
        path.join(__dirname, 'public', 'style.css'),
        'utf-8'
      );
      res.writeHead(200, {'Content-Type': 'text/css'});
      res.end(content);
    } catch (err) {
      console.log(err);
      res.writeHead(500, {'Content-Type': 'text/plain'});
      res.end('Internal Server Error');
    }
  } else {
    res.writeHead(404, {'Content-Type': 'text/plain'});
    res.end('Not Found');
  }
});

app.listen(3000, function() {
  console.log('The server has started.');
});

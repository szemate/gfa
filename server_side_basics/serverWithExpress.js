'use strict';

const express = require('express');
const path = require('path');

const app = express();

app.set('view engine', 'ejs'); // config setting

app.use(function(req, res, next) { // middleware
  console.log(req.method, req.url);
  next();
});

app.use('/static', express.static('public'));

app.get('/hello', function(req, res) {
  res.sendFile(path.join(__dirname, 'public', 'helloWorld.html'));
});

app.get('/hello2', function(req, res) {
  res.redirect('/static/helloWorld.html');
});

app.get('/hello/:name', function(req, res) {
  const { name } = req.params;
  let { gender } = req.query;

  if (!['male', 'female'].includes(gender)) {
    gender = null;
  }

  res.render('hello', { name, gender });
});

app.listen(3000, function() {
  console.log('The server has started.');
});

const express = require('express');
const mysql = require('mysql');

const port = 3000;

const app = express();

const conn = mysql.createConnection({
    host: '0.0.0.0',
    user: 'root',
    password: 'abc123',
    database: 'bookstore',
});

app.use(express.static('public'));

app.get('/', (req, res) => {
    res.redirect('/index.html');
});

app.get('/api/books', (req, res) => {
    const query = `
        SELECT book_name, aut_name, cate_descrip, pub_name, book_price
        FROM book_mast JOIN author ON book_mast.aut_id = author.aut_id
        JOIN category ON book_mast.cate_id = category.cate_id
        JOIN publisher ON book_mast.pub_id = publisher.pub_id
    `;

    conn.query(query, (err, rows) => {
        if (err) {
            console.error(err);
            res.status(500).send({ message: err.sqlMessage });
            return;
        }
        res.send({ books: rows });
    });
});

conn.connect((err) => {
    if (err) {
        console.error(err);
        return;
    }
    console.log('Connection established');
});

app.listen(port, () => {
  console.log(`Listening on port ${port}`);
});

const acceptJSON = (req, res, next) => {
    res.set('Accept', 'application/json');
    next();
};

module.exports = acceptJSON;

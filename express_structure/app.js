const express = require('express');
const db = require('./db');
const acceptJSONMiddleware = require('./middleware/acceptJSON');
const booksRoute = require('./routes/books');

const port = 3000;

const app = express();

app.use(acceptJSONMiddleware);

app.use(booksRoute);

app.listen(port, () => {
  console.log(`Listening on port ${port}`);
});

db.connect((err) => {
    if (err) {
        console.error(err);
        return;
    }
    console.log('Connection established');
});

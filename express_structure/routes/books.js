const express = require('express');
const db = require('../db');

const router = express.Router();

router.get('/books', (req, res) => {
    const query = `
        SELECT book_name, aut_name, cate_descrip, pub_name, book_price
        FROM book_mast JOIN author ON book_mast.aut_id = author.aut_id
        JOIN category ON book_mast.cate_id = category.cate_id
        JOIN publisher ON book_mast.pub_id = publisher.pub_id
        WHERE cate_descrip LIKE ? AND pub_name LIKE ?
    `;
    const params = [
        req.query.category || '%',
        req.query.publisher || '%',
    ];

    db.query(query, params, (err, rows) => {
        if (err) {
            console.error(err);
            res.status(500).send({ message: err.sqlMessage });
            return;
        }
        res.send({ books: rows });
    });
});

module.exports = router;
